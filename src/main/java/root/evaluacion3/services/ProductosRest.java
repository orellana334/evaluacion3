/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacion3.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.evaluacion3.dao.ProductosJpaController;
import root.evaluacion3.dao.exceptions.NonexistentEntityException;
import root.evaluacion3.entity.Productos;

/**
 *
 * @author AOrellana
 */

@Path("productos")
public class ProductosRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaProductos(){
    
        ProductosJpaController dao = new ProductosJpaController();
        List<Productos> productos = dao.findProductosEntities();
    
        return Response.ok(200).entity(productos).build();
    
    
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response ingresarStock(Productos producto){
    
        
    ProductosJpaController dao = new ProductosJpaController();
        try {
            dao.create(producto);
        } catch (Exception ex) {
            Logger.getLogger(ProductosRest.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    return Response.ok(200).entity(producto).build();
    
    
    }
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarStock(Productos producto) {

    ProductosJpaController dao = new ProductosJpaController();
        
        try {
            dao.edit(producto);
        } catch (Exception ex) {
            Logger.getLogger(ProductosRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(producto).build();
        
    }

    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarStock(@PathParam("ideliminar")String ideliminar) {

        ProductosJpaController dao = new ProductosJpaController();
        
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ProductosRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok("Se ha eliminado un producto del Stock").build();


    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{idconsulta}")
    public Response consultaProducto(@PathParam("idconsulta")String idconsulta){
    
    ProductosJpaController dao = new ProductosJpaController();
    
    Productos producto = dao.findProductos(idconsulta); 
    
    
    return Response.ok(200).entity(producto).build();
    
    
    }
    
}
