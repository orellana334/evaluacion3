<%-- 
    Document   : index
    Created on : 05-07-2021, 16:23:11
    Author     : AOrellana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Stock productos</h1>
        <h1>DETALLE Y STOCK PRODUCTOS - GET -https://evaluacion003.herokuapp.com/api/productos</h1>
        <h1>DETALLE Y STOCK PRODUCTOS por id - GET -https://evaluacion003.herokuapp.com/api/productos/{id}</h1>
        <h1>DETALLE Y STOCK PRODUCTOS - POST -https://evaluacion003.herokuapp.com/api/productos</h1>
        <h1>DETALLE Y STOCK PRODUCTOS - PUT -https://evaluacion003.herokuapp.com/api/productos</h1>
        <h1>DETALLE Y STOCK PRODUCTOS - DELETE -https://evaluacion003.herokuapp.com/api/productos/{id}</h1>
    </body>
</html>
